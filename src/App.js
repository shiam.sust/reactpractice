import React, { Component } from 'react';
import './App.css';
import Book from './Book/Book';

class App extends Component{

  state = {
    books:[
      {id:'fjsd',title:'Basic Math',author:'Khairul'},
      {id:'sfgv',title:'Programming with c',author:'Byron gotfried'},
      {id:'vfvd',title:'Eso programming shikhi',author:'Subeen'},
      {id:'fvwe',title:'Mp3 Bangla',author:'Shakhawat'},
      {id:'dsfv',title:'Mp3 Bangladesh',author:'George'}
    ],
    showBooks: true,
    showAddBook: false
  }

  deleteBookHandler = (bookIndex) => {
    //console.log(bookIndex);
    const tempBooks = this.state.books;
    tempBooks.splice(bookIndex,1);
    this.setState({books: tempBooks});
  }

  toggleBookHandler = () => {
    const doesShow = this.state.showBooks;
    this.setState({showBooks: !doesShow});
  }

  render() {
    let books = null;
    if(this.state.showBooks){
      books = (
        <div>
            {this.state.books.map((book, index) =>{
              return <Book 
                click={() => this.deleteBookHandler(index)}
                name={book.title}
                author={book.author}
                key={book.id}
              />
            })}    
        </div>
      );
    }

    return (
      <div className="App">
        <h1>Welcome to ReactJS Practice Session</h1>
        <h3>List of all Books</h3>
        <button onClick={this.toggleBookHandler}>Books</button>
        {books}
      </div>
    );
  }
}

export default App;
