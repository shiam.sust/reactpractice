import React from 'react';
import './Book.css';

const book = (props) => {
    return(
        <div className='Book'>
            <p>Book Title: {props.name}</p>
            <p>Book Author: {props.author}</p>
            <button onClick={props.click}>Delete</button>
        </div>
    );
}

export default book;